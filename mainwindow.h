#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QUdpSocket>

QT_BEGIN_NAMESPACE
namespace Ui
{
class MainWindow;
}
QT_END_NAMESPACE

class QListWidgetItem;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget* parent = nullptr);
    ~MainWindow();

private slots:
    void on_pushButton_clicked();

    void readSocket();

    void on_listWidget_itemClicked(QListWidgetItem* item);

private:
    Ui::MainWindow* ui;
    QUdpSocket searchSocket;

    void showMessage(const QString& msg);
};
#endif // MAINWINDOW_H
