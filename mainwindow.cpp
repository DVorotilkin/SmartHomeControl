#include "mainwindow.h"

#include <QDesktopServices>
#include <QKeyEvent>
#include <QMap>
#include <QNetworkDatagram>
#include <QRegularExpression>

#include "./ui_mainwindow.h"

namespace
{
const QByteArray searchRequest = "Hello?\r\n";
constexpr uint16_t reqestPort = 31415;
const QString responsPattern = "I'm here\\.\\s<(.*)>\\s*";

const int nameRole = Qt::UserRole + 1;
const int ipRole = Qt::UserRole + 2;
} // namespace

MainWindow::MainWindow(QWidget* parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(&searchSocket, &QUdpSocket::readyRead, this, &MainWindow::readSocket);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    searchSocket.writeDatagram(::searchRequest.data(), ::searchRequest.size(),
                               QHostAddress::Broadcast, ::reqestPort);
}

void MainWindow::readSocket()
{
    while (searchSocket.hasPendingDatagrams())
    {
        auto responce = searchSocket.receiveDatagram();
        auto deviceIp = responce.destinationAddress().toString();
        QRegularExpression regexp(::responsPattern);
        auto match = regexp.match(responce.data());
        if (!match.hasMatch())
        {
            showMessage(QString("Response %1 dosn't math").arg(responce.data()));
            return;
        }

        QString name = match.captured(1);

        auto item = new QListWidgetItem(QString("%1 (%2)").arg(name).arg(deviceIp), ui->listWidget);

        item->setData(::nameRole, name);
        item->setData(::ipRole, deviceIp);

        ui->listWidget->addItem(QString("%1 (%2)").arg(name).arg(deviceIp));
    }
}

void MainWindow::on_listWidget_itemClicked(QListWidgetItem* item)
{
    auto deviceIp = item->data(::ipRole).toString();
    showMessage(QString("Opening %1 ...").arg(deviceIp));
    QDesktopServices::openUrl(QUrl(deviceIp));
}

void MainWindow::showMessage(const QString& msg)
{
    qDebug() << msg;
    statusBar()->showMessage(msg);
}
